# frozen_string_literal: true

require 'tty-prompt'

class Repl
  def initialize
    @prompt = TTY::Prompt.new
  end

  def select(title, options)
    @prompt.select(title, options)
  end

  def print(title)
    @prompt.ok(title)
  end

  def yes?(title)
    @prompt.yes?(title)
  end
end
