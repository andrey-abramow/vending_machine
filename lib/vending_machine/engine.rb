# frozen_string_literal: true

require_relative './purchase'
class Engine
  attr_reader :purchase

  def initialize(coins_holder, products_holder)
    @coins_holder = coins_holder
    @products_holder = products_holder
  end

  def products
    @products_holder.products
  end

  def open_purchase(product)
    @purchase = Purchase.new(product)
  end

  def supported_coins
    @coins_holder.supported_coins
  end

  def inserted_amount
    @purchase.inserted_amount
  end

  def amount_to_pay
    @purchase.amount_to_pay
  end

  def insert_coin(coin)
    @coins_holder.insert_coin(coin)
    @purchase.add_coin(coin)

    close_purchase if @purchase.enough_money_inserted?
  end

  def wating_for_payment?
    !@purchase.enough_money_inserted?
  end

  private

  def close_purchase
    if @coins_holder.can_withdraw? @purchase.change_amount
      succeed_purchase
    else
      no_change_fail
    end
  end

  def succeed_purchase
    @coins_holder.accept_inserted_coins
    @products_holder.give_product(@purchase.product)
    change_coins = @coins_holder.withdraw(@purchase.change_amount)
    @purchase.complete_with_change(change_coins)
  end

  def no_change_fail
    @coins_holder.reject_inserted_coins
    @purchase.no_change_fail
  end
end
