# frozen_string_literal: true

class Machine
  def initialize(ui, engine)
    @ui = ui
    @engine = engine
  end

  def start
    new_session

    new_session while @ui.to_continue?

    @ui.bye!
  end

  def new_session
    @product = @ui.select_product(products)

    if @product
      purchase(@product)
      print_up_purchase(@engine.purchase)
    else
      @ui.error
    end
  end

  private

  def products
    @engine.products
  end

  def purchase(product)
    @engine.open_purchase(product)
    process_payment
  end

  def process_payment
    while @engine.wating_for_payment?
      @ui.show_payment_status(@engine.inserted_amount, @engine.amount_to_pay)
      coin = @ui.select_coin(@engine.supported_coins)
      @engine.insert_coin(coin) if coin
    end
  rescue StandardError
    # can be unsupported coin, skip for now as UI does not allow this
  end

  def print_up_purchase(purchase)
    if purchase.succeded?
      @ui.product_sold(purchase.product)
      @ui.take_your_change(purchase.change_coins) unless purchase.change_coins.empty?
    else
      @ui.no_change_decline
    end
  end
end
