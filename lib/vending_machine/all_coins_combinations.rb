# frozen_string_literal: true

require 'pry'
class PayAmountWithCoin
  def initialize(amount, coin, set)
    @amount = amount
    @coin = coin
    @set = set
  end

  def coin_count
    @set[@coin]
  end

  def coins_count_to_take
    @coins_count_to_take = (@amount * 100).to_i / (@coin * 100).to_i
    @coins_count_to_take = coin_count if @coins_count_to_take > coin_count
    @coins_count_to_take
  end

  def next_set
    @next_set = @set.clone
    @next_set[@coin] = @next_set[@coin] - coins_count_to_take
    @next_set
  end

  def taken_set
    @taken_set ||= { @coin => coins_count_to_take }
  end

  def amount_left
    @amount_left ||= @amount - coins_count_to_take * @coin
  end
end

class AllCoinsCombinations
  def initialize(money, coins)
    @money = money
    @coins = coins
  end

  def call
    @rusult ||= find_combinations(@money, @coins)
  end

  def find_combinations(amount, coins = [])
    all_combinations = []
    coins.keys.each_with_index do |coin, _i|
      next if (coins[coin]).zero?

      next unless coin <= amount

      result = PayAmountWithCoin.new(amount, coin, coins)
      further_combs = find_combinations(result.amount_left, result.next_set)
      if further_combs.empty? && result.amount_left.zero?
        all_combinations.push(result.taken_set)
      else
        add_subset_to_sets(further_combs, result.taken_set)
        all_combinations.concat(further_combs)
      end
    end
    all_combinations.uniq
  end

  def add_subset_to_sets(sets, subset)
    sets.each do |set|
      (set.keys + subset.keys).uniq.each do |k|
        set[k] = subset[k] || 0 + set[k] || 0
      end
    end
  end
end
