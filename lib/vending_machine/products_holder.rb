# frozen_string_literal: true

class ProductsHolder
  attr_reader :products

  def initialize(products)
    @products = products
  end

  def give_product(product)
    raise 'No products left' if product[:count].zero?

    product[:count] -= 1
  end
end
