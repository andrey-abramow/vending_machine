# frozen_string_literal: true

require_relative './withdraw_service'

SORT_BY_VALUE = proc { |item1, item2| item1.sum { |_v, k| k } <=> item2.sum { |_v, k| k } }
class CoinsHolder
  attr_reader :inserted_coins_set

  def initialize(coins)
    @coins = coins
    @inserted_coins_set = {}
  end

  def insert_coin(coin)
    raise "Unsupported coin: #{coin}" unless supported_coins.include?(coin)

    @inserted_coins_set[coin] = (@inserted_coins_set[coin] || 0) + 1
  end

  def inserted_coins
    @inserted_coins_set.flat_map { |k, v| v.times.map { k } }
  end

  def supported_coins
    @coins.keys
  end

  def can_withdraw?(amount)
    WithdrawService.new(all_coins, amount).can_withdraw?
  end

  def reject_inserted_coins
    coins = inserted_coins
    @inserted_coins_set = {}
    coins
  end

  def accept_inserted_coins
    to_add = @inserted_coins_set
    @coins.each do |k, _v|
      @coins[k] = @coins[k] + (to_add[k] || 0)
    end
    @inserted_coins_set = {}
    to_add
  end

  def withdraw(amount)
    to_withdraw = WithdrawService.new(@coins, amount).to_withdraw
    @coins.each do |k, _v|
      @coins[k] = @coins[k] - (to_withdraw[k] || 0)
    end
    to_withdraw
  end

  private

  def all_coins
    all = {}
    @coins.each do |k, _v|
      all[k] = @coins[k] + (@inserted_coins_set[k] || 0)
    end
    all
  end
end
