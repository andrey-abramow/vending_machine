# frozen_string_literal: true

require_relative './all_coins_combinations'

class WithdrawService
  def initialize(coins, amount)
    @coins = coins
    @amount = amount
  end

  def can_withdraw?
    @amount.zero? || !less_coin_combination.nil?
  end

  def to_withdraw
    less_coin_combination || {}
  end

  def less_coin_combination
    @less_coin_combination ||= possible_combinations.min(&SORT_BY_VALUE)
  end

  def possible_combinations
    @possible_combinations ||= AllCoinsCombinations.new(@amount, @coins).call
  end
end
