# frozen_string_literal: true

class Purchase
  attr_reader :change_coins, :status, :product

  STATUS = [IN_PROGRESS = 0, SUCCESS = 1, DECLINED = 2, NO_CHANGE = 3].freeze

  def initialize(product)
    @product = product
    @inserted_coins = []
  end

  def cost
    @product[:price]
  end

  def enough_money_to_pay?
    inserted_amount >= @product[:price]
  end

  def add_coin(coin)
    @inserted_coins.push(coin)
  end

  def inserted_amount
    @inserted_coins.sum
  end

  def enough_money_inserted?
    cost <= inserted_amount
  end

  def amount_to_pay
    cost - inserted_amount
  end

  def change_amount
    inserted_amount - cost
  end

  def succeded?
    @status == Purchase::SUCCESS
  end

  def no_change?
    @status == Purchase::NO_CHANGE
  end

  def complete_with_change(change_coins)
    @change_coins = change_coins
    @status = Purchase::SUCCESS
  end

  def fail_with_change
    @change_coins = @inserted_coins
    @status = Purchase::NO_CHANGE
  end
end
