# frozen_string_literal: true

require_relative './repl'

class UI
  def initialize(repl)
    @repl = repl
  end

  def select_product(products)
    product_option_mapper = proc do |product|
      {
        name: "#{product[:name]} - #{product[:price]}",
        value: product[:name],
        disabled: (product[:count]).zero? ? 'Out of stock' : false
      }
    end
    options = products.map(&product_option_mapper)

    product_name = @repl.select('Select product', options)
    products.find { |p| p[:name] == product_name }
  end

  def show_payment_status(money_inserted, money_left)
    @repl.print("Inserted: #{money_inserted}, left to pay: #{money_left}")
  end

  def select_coin(supported_coins)
    options = supported_coins
    @repl.select('Please insert a coin', supported_coins)
  end

  def product_sold(name)
    @repl.print("Operation successfull, enjoy your #{name}!")
  end

  def take_your_change(coins)
    @repl.print("And do not forget the change: #{coins}")
  end

  def to_continue?
    @repl.yes?('Do you want to buy something else?')
  end

  def bye!
    @repl.print('Good buy')
  end

  def no_change_decline
    @repl.print('Sorry, transaction aborted. No money to change')
  end
end
