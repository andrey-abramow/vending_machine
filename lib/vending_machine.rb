# frozen_string_literal: true

Dir['./lib/vending_machine/*.rb'].sort.each { |file| require file }

class VendingMachine
  def initialize(coins_set, product_list)
    @coins_set = coins_set
    @product_list = product_list
    @machine = Machine.new(UI.new(Repl.new),
                           Engine.new(CoinsHolder.new(@coins_set), ProductsHolder.new(@product_list)))
  end

  def start
    @machine.start
  end
end
