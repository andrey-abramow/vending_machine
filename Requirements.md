#
https://docs.google.com/document/d/17jsoKJtzMPf84qG61MQpZqScIbRDXZ9QwnzK1hGjmBU/edit#
# N-iX #RoR\_challenge

Instructions:

The challenge is about participation in the software development contest using Ruby technology. The task is to develop a virtual vending machine application which should emulate the goods buying operations and change calculation.

Following are the functional and technical requirements:

- Allow user to select a product from a provided machine&#39;s inventory;
- Allow user to insert coins into a vending machine;
- Once the product is selected and the appropriate amount of coins inserted - it should return the product;

- It should return change (coins) if inserted too much;
- Change should be returned with the minimum amount of coins possible;
- It should notify the customer when the selected product is out of stock;
- It should return inserted coins in case it does nothave enough change.

This task should be implemented as a **CLI application** using only **Ruby** programming language. You are free to use any additional Gems you like, though the usage should be justified. The code should be readable, easily maintainable and following best practices (_\*hint: the application will benefit from a well structured_ _ **OOP** _ _approach ;)_ )

You also have to make sure the code really works, use any tool or technique you need to accomplish this. It is expected to see some **Unit Tests** around as well.

To sum up, you can use the **task acceptance criteria** as follows:

- Application generates expected result on any input;
- Application is a Ruby CLI application;
- Don&#39;t add gems that you wouldn&#39;t really need;
- Structure the code under OOP paradigm;
- Make robust, readable classes;
- Ensure proper unit test coverage;
- Make sure the application works and covers corner cases.

Initial Input:

The vending machine should be initialized with the following inventory (yet, have in mind the input may vary):

| **Product Name** | **Price** | **Quantity** |
| --- | --- | --- |
| Coca Cola | 2.00 | 2 |
| Sprite | 2.50 | 2 |
| Fanta | 2.70 | 3 |
| Orange Juice | 3.00 | 1 |
| Water | 3.25 | 0 |

The vending machine should be Initialized with the following set of coins in till:

| **Value** | **Quantity** |
| --- | --- |
| 5.00 | 5 |
| 3.00 | 5 |
| 2.00 | 5 |
| 1.00 | 5 |
| 0.50 | 5 |
| 0.25 | 5 |

Submission:

Please write some code and write a README with a guide of how to run it.

Either send us a link to the repository on somewhere like _github_ or _bitbucket_ (bitbucket has free private repositories) or send us a _git bundle_.

git bundle create vending-machine-test.bundle master

Please also send us the resulting vending-machine-test.bundle file.

This .bundle file can be cloned using:

git bundle clone bundle-filename.bundle -b master directory-name

_ **\* Please notice, the deadline for the task is 2 weeks from the date of receiving a message** _ **.**

**Good Luck!**