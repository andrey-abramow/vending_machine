# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Machine do
  describe '#start' do
    it 'Should open new session by default' do
      ui = UITest.new
      engine = EngineTest.new

      machine = Machine.new(ui, engine)

      expect(machine).to receive(:new_session)
      machine.start
    end

    it 'Should start a new session after previous one' do
      ui = UITestWithContinue.new
      engine = EngineTest.new
      machine = Machine.new(ui, engine)

      expect(machine).to receive(:new_session).exactly(UITestWithContinue::TIMES_TO_CONTINUE).times
      machine.start
    end

    it 'Should call ui.bye! at the end' do
      ui = UITestWithContinue.new
      engine = EngineTest.new
      machine = Machine.new(ui, engine)

      class << machine
        define_method(:new_session, proc {})
      end

      expect(ui).to receive(:bye!).once
      machine.start
    end
  end

  describe '#new_session' do
    it 'Should proceed with purchase if product selected' do
      engine = EngineTest.new

      ui = UITest.new

      machine = Machine.new(ui, engine)

      class << machine
        define_method(:print_up_purchase, proc { |_purchase| nil })
      end

      expect(machine).to receive(:purchase).with(PRODUCTS[0])
      machine.new_session
    end

    it 'Should print up results after purchase' do
      engine = EngineTest.new

      ui = UITest.new

      machine = Machine.new(ui, engine)

      class << machine
        define_method(:purchase, proc { |_product| nil })
      end

      expect(machine).to receive(:print_up_purchase)
      machine.new_session
    end

    it 'Should trigger an error in UI if no product' do
      ui = UITest.new
      class << ui
        define_method(:select_product, proc { |_products| nil })
      end

      engine = EngineTest.new
      machine = Machine.new(ui, engine)

      expect(ui).to receive(:error).once
      machine.new_session
    end
  end

  describe '#purchase' do
    it 'Should open purchase on engine, process payment and show results at' do
      engine = EngineTest.new

      ui = UITest.new

      machine = Machine.new(ui, engine)

      expect(engine).to receive(:open_purchase).with(PRODUCTS[0])
      expect(machine).to receive(:process_payment)
      machine.send(:purchase, PRODUCTS[0])
    end

    it 'Should trigger an error in UI if no product' do
      ui = UITest.new
      class << ui
        define_method(:select_product, proc { |_products| nil })
      end

      engine = EngineTest.new
      machine = Machine.new(ui, engine)

      expect(ui).to receive(:error).once
      machine.new_session
    end
  end

  describe '#process_payment' do
    it 'Should show payment status on UI if waiting for payment' do
      engine = EngineWithWaitForPaymentTest.new

      class << engine
        define_method(:inserted_amount, proc { 10 })
        define_method(:amount_to_pay, proc { 20 })
      end

      ui = UITest.new

      class << ui
        define_method(:select_coin, proc { |_coins| nil })
      end

      machine = Machine.new(ui, engine)

      expect(ui).to receive(:show_payment_status).with(engine.inserted_amount, engine.amount_to_pay)
      machine.send(:process_payment)
    end

    it 'Should add coin to engine if coin provided' do
      engine = EngineWithWaitForPaymentTest.new

      class << engine
        define_method(:inserted_amount, proc { 10 })
        define_method(:amount_to_pay, proc { 20 })
      end

      ui = UITest.new

      class << ui
        define_method(:select_coin, proc { |_coins| 5 })
        define_method(:show_payment_status, proc { |inserted_amount, amount_to_pay| })
      end

      machine = Machine.new(ui, engine)

      expect(engine).to receive(:insert_coin).with(5)
      machine.send(:process_payment)
    end
  end

  describe '#print_up_purchase' do
    it 'Should print about product if purchase is successful' do
      engine = EngineWithWaitForPaymentTest.new

      ui = UITest.new

      purchase = Purchase.new('Product')

      class << purchase
        define_method(:succeded?, proc { true })
        define_method(:change_coins, proc { [] })
      end

      machine = Machine.new(ui, engine)

      expect(ui).to receive(:product_sold).with(purchase.product)
      expect(ui).not_to receive(:take_your_change).with(purchase.change_coins)

      machine.send(:print_up_purchase, purchase)
    end

    it 'Should print change coins if there is a change' do
      engine = EngineWithWaitForPaymentTest.new

      ui = UITest.new

      purchase = Purchase.new('Product')

      class << ui
        define_method(:product_sold, proc { |product| })
      end

      class << purchase
        define_method(:succeded?, proc { true })
        define_method(:change_coins, proc { [1, 2] })
      end

      machine = Machine.new(ui, engine)

      expect(ui).to receive(:take_your_change).with(purchase.change_coins)
      machine.send(:print_up_purchase, purchase)
    end

    it 'Should print returned coins if purchase unsuccessful' do
      engine = EngineWithWaitForPaymentTest.new

      ui = UITest.new

      purchase = Purchase.new('Product')

      class << purchase
        define_method(:succeded?, proc { false })
        define_method(:change_coins, proc { [1, 2] })
      end

      machine = Machine.new(ui, engine)

      expect(ui).to receive(:no_change_decline)
      machine.send(:print_up_purchase, purchase)
    end
  end
end
