# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Purchase do
  describe '.new' do
    it 'Should be instantiated with product' do
      expect { Purchase.new(PRODUCTS[0]) }.not_to raise_error(ArgumentError)
    end
  end

  describe '#inserted_amount' do
    it 'Should return inserted amount' do
      purchase = Purchase.new('Product')
      coins = [5, 5]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.inserted_amount).to eql(coins.sum)
    end
  end

  describe '#amount_to_pay' do
    it 'Should return amount_to_pay' do
      purchase = Purchase.new({ name: 'Product', price: 5 })
      coins = [4]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.amount_to_pay).to eql(1)
    end
  end

  describe '#add_coin' do
    it 'Should add coin to inserted coins' do
      purchase = Purchase.new({ name: 'Product', price: 5 })
      coins = [5, 5]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.instance_variable_get('@inserted_coins')).to eql(coins)
    end
  end

  describe '#enough_money_inserted?' do
    it 'Should return true if exact money inserted' do
      purchase = Purchase.new({ name: 'Product', price: 5 })
      coins = [5]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.enough_money_inserted?).to eql(true)
    end

    it 'Should return true if more money inserted' do
      purchase = Purchase.new({ name: 'Product', price: 5 })
      coins = [6]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.enough_money_inserted?).to eql(true)
    end

    it 'Should return false if not enough money inserted' do
      purchase = Purchase.new({ name: 'Product', price: 10 })
      coins = [5]
      coins.each { |coin| purchase.add_coin(coin) }
      expect(purchase.enough_money_inserted?).to eql(false)
    end
  end

  describe '#complete_with_change' do
    it 'Should set success status and change coins' do
      purchase = Purchase.new({ name: 'Product', price: 10 })
      purchase.complete_with_change([])
      expect(purchase.status).to eql(Purchase::SUCCESS)
    end
  end

  describe '#fail_with_change' do
    it 'Should set NO_CHANGE status and change coins to inserted' do
      purchase = Purchase.new({ name: 'Product', price: 10 })
      purchase.fail_with_change
      expect(purchase.status).to eql(Purchase::NO_CHANGE)
    end
  end

  describe '#enough_money_to_pay?' do
    it 'Should return true if inserted amount more than cost' do
      purchase = Purchase.new({ name: 'Product', price: 10 })
      purchase.instance_variable_set('@inserted_coins', [5, 5])
      expect(purchase.enough_money_to_pay?).to eql(true)
    end

    it 'Should return false if inserted amount less than cost' do
      purchase = Purchase.new({ name: 'Product', price: 20 })
      purchase.instance_variable_set('@inserted_coins', [5])
      expect(purchase.enough_money_to_pay?).to eql(false)
    end
  end
end
