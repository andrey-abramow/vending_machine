# frozen_string_literal: true

require 'spec_helper'

RSpec.describe VendingMachine do
  describe '.new' do
    it 'Should take product list and coins as args' do
      service = VendingMachine.new({}, {})
      expect { VendingMachine.new({}, {}) }.not_to raise_error
    end
  end

  describe '#start' do
    it 'Should call start on machine' do
      service = VendingMachine.new({}, {})
      machine = service.instance_variable_get('@machine')
      expect(machine).to receive(:start)
      machine.start
    end
  end
end
