# frozen_string_literal: true

require 'spec_helper'

RSpec.describe WithdrawService do
  def can_withdraw?
    !less_coin_combination.nil?
  end

  def to_withdraw
    less_coin_combination
  end

  describe '#can_withdraw?' do
    it 'Should return true if amount is zero' do
      service = WithdrawService.new({}, 0)
      expect(service.can_withdraw?).to eql(true)
    end
  end

  describe '#to_withdraw' do
    it 'Should return empty set if no combination' do
      service = WithdrawService.new({}, 0)
      expect(service.to_withdraw).to eql({})
    end
  end
end
