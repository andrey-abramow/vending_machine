# frozen_string_literal: true

require 'spec_helper'

RSpec.describe UI do
  describe '#select_product' do
    it('Should call repl with proper title and options') do
      TEST_CASES = [
        {
          products: [{ name: 'Product', price: 1.00, count: 1 }],
          title: 'Select product',
          options: [{ disabled: false, name: 'Product - 1.0', value: 'Product' }]
        },
        {
          products: [{ name: 'Product', price: 1.00, count: 0 }],
          options: [{ disabled: 'Out of stock', name: 'Product - 1.0', value: 'Product' }]
        }
      ].each do |test_case|
        repl = ReplTest.new
        ui = UI.new(repl)
        expect(repl).to receive(:select).with('Select product', test_case[:options])
        ui.select_product(test_case[:products])
      end
    end
  end

  describe '#no_change_decline' do
    it('Should call repl with: Sorry, transaction aborted. No money to change') do
      repl = ReplTest.new
      ui = UI.new(repl)
      expect(repl).to receive(:print).with('Sorry, transaction aborted. No money to change')
      ui.no_change_decline
    end
  end

  describe '#to_continue?' do
    it('Should call repl.yes? with message: Do you want to buy something else?') do
      repl = ReplTest.new
      ui = UI.new(repl)
      expect(repl).to receive(:yes?).with('Do you want to buy something else?')
      ui.to_continue?
    end
  end

  describe '#take_your_change' do
    it('Should call repl.print with And do not forget the change: #coins') do
      repl = ReplTest.new
      ui = UI.new(repl)
      coins = [1, 2]
      expect(repl).to receive(:print).with("And do not forget the change: #{coins}")
      ui.take_your_change(coins)
    end
  end

  describe '#product_sold' do
    it('Should call repl.print with Operation successfull, enjoy your #prod_name!') do
      repl = ReplTest.new
      ui = UI.new(repl)
      prod_name = 'Cola'
      expect(repl).to receive(:print).with("Operation successfull, enjoy your #{prod_name}!")
      ui.product_sold(prod_name)
    end
  end

  describe '#select_coin' do
    it('Should call repl.select with title: Please insert a coin and options: #supported_coins') do
      repl = ReplTest.new
      ui = UI.new(repl)
      coins = [1]
      expect(repl).to receive(:select).with('Please insert a coin', coins)
      ui.select_coin(coins)
    end
  end

  describe '#show_payment_status' do
    it('Should call repl.print with: Inserted: #money_inserted, left to pay: #money_left') do
      repl = ReplTest.new
      ui = UI.new(repl)
      money_inserted, money_left = 1
      expect(repl).to receive(:print).with("Inserted: #{money_inserted}, left to pay: #{money_left}")
      ui.show_payment_status(money_inserted, money_left)
    end
  end
end
