# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Engine do
  describe '#purchase' do
    it 'Should return current purchase' do
      products_holder = ProductsHolderTest.new
      products_holder.define_singleton_method(:products, proc { PRODUCTS })
      coins_holder = CoinsHolderTest.new

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', true)

      expect(engine.purchase).to eql(true)
    end
  end

  describe '#products' do
    it 'Should return products from products holder' do
      products_holder = ProductsHolderTest.new
      products_holder.define_singleton_method(:products, proc { PRODUCTS })
      coins_holder = CoinsHolderTest.new

      engine = Engine.new(coins_holder, products_holder)

      expect(engine.products).to equal(products_holder.products)
    end
  end

  describe '#open_purchase' do
    it 'Should create purchase object' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      engine = Engine.new(coins_holder, products_holder)

      product = PRODUCTS[0]
      engine.open_purchase(product)
      expect(product).to eql(engine.instance_variable_get('@purchase').product)
    end
  end

  describe '#supported_coins' do
    it 'Should return supported coins from coins holder' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new
      coins_holder.define_singleton_method(:supported_coins, proc { [1, 2] })

      engine = Engine.new(coins_holder, products_holder)

      expect(engine.supported_coins).to eql(coins_holder.supported_coins)
    end
  end

  describe '#insert_coin' do
    it 'Should add coin to coins holder and to purchase' do
      coin = 5
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      engine = Engine.new(coins_holder, products_holder)

      purchase = PurchaseTest.new

      engine.instance_variable_set('@purchase', purchase)

      expect(coins_holder).to receive(:insert_coin).with(coin)
      expect(purchase).to receive(:add_coin).with(coin)
      engine.insert_coin(coin)
    end

    it 'Should close purchase if enough money inserted' do
      coin = 5
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      engine = Engine.new(coins_holder, products_holder)

      purchase = Purchase.new(PRODUCTS[0])
      class << purchase
        define_method(:enough_money_inserted?, proc { true })
      end

      engine.instance_variable_set('@purchase', purchase)

      expect(engine).to receive(:close_purchase).with(no_args)
      engine.insert_coin(coin)
    end
  end

  describe '#close_purchase' do
    it 'Should call succeed if can withdraw change' do
      products_holder = ProductsHolderTest.new

      coins_holder = CoinsHolderTest.new

      class << coins_holder
        define_method(:can_withdraw?, proc { |_amount| true })
      end

      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)

      engine.instance_variable_set('@purchase', purchase)

      expect(engine).to receive(:succeed_purchase)
      engine.send(:close_purchase)
    end

    it 'Should call fail_purchase if cannot withdraw' do
      products_holder = ProductsHolderTest.new

      coins_holder = CoinsHolderTest.new

      class << coins_holder
        define_method(:can_withdraw?, proc { |_amount| false })
      end

      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)

      engine.instance_variable_set('@purchase', purchase)

      expect(engine).to receive(:no_change_fail)
      engine.send(:close_purchase)
    end
  end

  describe '#succeed_purchase' do
    it 'Should keep inserted coins' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new
      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(coins_holder).to receive(:accept_inserted_coins)
      engine.send(:succeed_purchase)
    end

    it 'Should give avay the product from products holder' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new
      purchase = PurchaseTest.new
      purchase.instance_variable_set('@product', PRODUCTS[0])

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(products_holder).to receive(:give_product).with(PRODUCTS[0])
      engine.send(:succeed_purchase)
    end

    it 'Should withdraw change from coins holder' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      amount = 10
      purchase = PurchaseTest.new
      purchase.define_singleton_method(:change_amount, proc { amount })

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(coins_holder).to receive(:withdraw).with(amount)
      engine.send(:succeed_purchase)
    end

    it 'Should complete purchase with change coins' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      change_coins = [1, 2]
      coins_holder.define_singleton_method(:withdraw, proc { |_amount| change_coins })

      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(purchase).to receive(:complete_with_change).with(change_coins)
      engine.send(:succeed_purchase)
    end
  end

  describe '#no_change_fail' do
    it 'Should reject inserted coins from coins holder' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(coins_holder).to receive(:reject_inserted_coins)
      engine.send(:no_change_fail)
    end

    it 'Should call no_change_fail on purchase' do
      products_holder = ProductsHolderTest.new
      coins_holder = CoinsHolderTest.new

      purchase = PurchaseTest.new

      engine = Engine.new(coins_holder, products_holder)
      engine.instance_variable_set('@purchase', purchase)

      expect(purchase).to receive(:no_change_fail)
      engine.send(:no_change_fail)
    end
  end
end
