# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ProductsHolder do
  describe '#products' do
    it 'Should return products' do
      products_holder = ProductsHolder.new(PRODUCTS)
      expect(products_holder.products).to eql(PRODUCTS)
    end
  end

  describe '#give_product' do
    it 'Should return product' do
      product = { name: 'P', count: 1, price: 1 }
      products_holder = ProductsHolder.new([product])
      products_holder.give_product(product)
      expect(product[:count]).to eql(0)
    end

    it 'Should raise an exception if no producs left' do
      product = { name: 'P', count: 0, price: 1 }
      products_holder = ProductsHolder.new([product])
      expect { products_holder.give_product(product) }.to raise_error
    end
  end
end
