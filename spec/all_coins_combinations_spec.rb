# frozen_string_literal: true

require 'spec_helper'

RSpec.describe AllCoinsCombinations do
  describe '#call' do
    TEST_CASES = [
      {
        amount: 10,
        coins_set: { 1 => 1, 2 => 1, 3 => 1, 4 => 1 },
        res: [{ 1 => 1, 2 => 1, 3 => 1, 4 => 1 }]
      },
      {
        amount: 10,
        coins_set: { 1 => 1, 2 => 1, 3 => 1, 4 => 1, 5 => 1 },
        res: [{ 4 => 1, 3 => 1, 2 => 1, 1 => 1 }, { 5 => 1, 4 => 1, 1 => 1 }, { 5 => 1, 3 => 1, 2 => 1 }]
      },
      {
        amount: 10,
        coins_set: { 4 => 4 },
        res: []
      },
      {
        amount: 15,
        coins_set: { 5 => 4 },
        res: [{ 5 => 3 }]
      },
      {
        amount: 15,
        coins_set: { 5 => 2 },
        res: []
      }
    ].each do |test_case|
      it "Should return #{test_case[:res]} for amount: #{test_case[:amount]}, coins_set: #{test_case[:coins_set]}}" do
        expect(AllCoinsCombinations.new(test_case[:amount], test_case[:coins_set]).call).to eql(test_case[:res])
      end
    end
  end
end
