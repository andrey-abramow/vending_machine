# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CoinsHolder do
  describe '#supported_coins' do
    it 'Should return supported coins' do
      COINS_SET = {
        1.00 => 5,
        0.50 => 0
      }.freeze
      coins_holder = CoinsHolder.new(COINS_SET)
      expect(coins_holder.supported_coins).to eql(COINS_SET.keys)
    end
  end

  describe '#insert_coin' do
    it 'Should accept inserted coin' do
      COINS_SET = {
        1.00 => 5,
        0.50 => 0
      }.freeze
      coins_holder = CoinsHolder.new(COINS_SET)
      expect { coins_holder.insert_coin(1.00) }.not_to raise_error
    end

    it 'Should raise an error if unsupported coin' do
      COINS_SET = {
        1.00 => 5,
        0.50 => 0
      }.freeze
      coins_holder = CoinsHolder.new(COINS_SET)
      expect { coins_holder.insert_coin(2.00) }.to raise_error(RuntimeError, 'Unsupported coin: 2.0')
    end
  end

  describe '#can_withdraw?' do
    it 'Should receive amount as argument' do
      coins_holder = CoinsHolder.new(COINS_SET)
      expect { coins_holder.can_withdraw?(5) }.not_to raise_error
    end
  end

  describe '#reject_inserted_coins' do
    it 'Should return inserted coins and clean up the inserted set' do
      coins_holder = CoinsHolder.new(COINS_SET)
      coins_holder.instance_variable_set('@inserted_coins_set', { 4 => 1 })
      expect(coins_holder.reject_inserted_coins).to eql([4])
      expect(coins_holder.instance_variable_get('@inserted_coins_set')).to eql({})
    end
  end

  describe '#accept_inserted_coins' do
    it 'Should add inserted coins to coins' do
      coins_holder = CoinsHolder.new({ 4 => 1, 5 => 0 })
      coins_holder.instance_variable_set('@inserted_coins_set', { 5 => 1, 4 => 1 })
      expect(coins_holder.accept_inserted_coins).to eql({ 5 => 1, 4 => 1 })
      expect(coins_holder.instance_variable_get('@coins')).to eql({ 4 => 2, 5 => 1 })
    end
  end

  describe '#withdraw' do
    it 'Should decrease coins on withdrawal amount' do
      coins_holder = CoinsHolder.new({ 4 => 1, 5 => 10 })
      expect(coins_holder.withdraw(10)).to eql({ 5 => 2 })
      expect(coins_holder.instance_variable_get('@coins')).to eql({ 4 => 1, 5 => 8 })
    end
  end
end
