# frozen_string_literal: true

require 'simplecov'
require 'pry'

SimpleCov.start do
  add_filter '/spec/'
end

require 'vending_machine'

PRODUCTS = [{ name: 'Coca Cola', price: 3.00, count: 2 }]
class ReplTest
  def select; end

  def print; end

  def yes?; end
end

class UITest
  def error; end

  def to_continue?
    false
  end

  def bye!; end

  def show_payment_status; end

  def select_product(_products)
    PRODUCTS[0]
  end
end

class UITestWithContinue
  TIMES_TO_CONTINUE = 5

  def initialize
    @times_to_continue = UITestWithContinue::TIMES_TO_CONTINUE
  end

  def to_continue?
    (@times_to_continue -= 1) > 0
  end

  def error; end

  def bye!; end

  def show_payment_status; end

  def select_product(product); end
end

class PurchaseTest
  attr_accessor :product

  def no_change_fail; end

  def complete_with_change(money); end

  def enough_money_to_pay?; end

  def add_coin(coin); end

  def inserted_amount; end

  def enough_money_inserted?; end

  def amount_to_pay; end

  def change_amount; end

  def succeded?; end

  def no_change?; end
end

class EngineTest
  def products
    PRODUCTS
  end

  def purchase
    PurchaseTest.new
  end

  def open_purchase(product); end

  def supported_coins; end
end

class EngineWithWaitForPaymentTest
  TIMES_TO_WAIT = 1

  def initialize
    @times_to_wait = EngineWithWaitForPaymentTest::TIMES_TO_WAIT
  end

  def wating_for_payment?
    (@times_to_wait -= 1) >= 0
  end

  def supported_coins; end
end

class CoinsHolderTest
  def insert_coin(coin); end

  def withdraw(amount); end

  def supported_coins
    [5.0, 4.0, 3.0]
  end

  def accept_inserted_coins; end

  def reject_inserted_coins; end
end

class ProductsHolderTest
  def products
    PRODUCTS
  end

  def give_product(product); end
end
