# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Repl do
  describe '#select' do
    it('Should call @prompt with args') do
      service = Repl.new
      args = [1, 2]
      expect(service.instance_variable_get('@prompt')).to receive(:select).with(*args)
      service.select(*args)
    end
  end

  describe '#print' do
    it('Should call @prompt.ok with title') do
      service = Repl.new
      title = 'Hello'
      expect(service.instance_variable_get('@prompt')).to receive(:ok).with(title)
      service.print(title)
    end
  end

  describe '#yes?' do
    it('Should call @prompt.yes? with title') do
      service = Repl.new
      title = 'Yes?'
      expect(service.instance_variable_get('@prompt')).to receive(:yes?).with(title)
      service.yes?(title)
    end
  end
end
