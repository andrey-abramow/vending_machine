# frozen_string_literal: true

require './lib/vending_machine'

DEFAULT_COINS_SET = {
  5.00 => 5,
  3.00 => 5,
  2.00 => 5,
  1.00 => 5,
  0.50 => 5,
  0.25 => 5
}

DEFAULT_PRODUCT_LIST = [
  { name: 'Coca Cola', price: 3.00, count: 2 },
  { name: 'Sprite', price: 2.50, count: 2 },
  { name: 'Fanta', price: 2.70, count: 3 },
  { name: 'Orange Juice', price: 3.00, count: 1 },
  { name: 'Water', price: 3.25, count: 0 }
]

VendingMachine.new(DEFAULT_COINS_SET, DEFAULT_PRODUCT_LIST).start
