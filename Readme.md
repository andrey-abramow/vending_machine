# Setup
- bundle install

# Tests / Coverage
- rake
- open coverage/index.html

# Run
 - ruby run.rb
